#include "Compara.h"

Compara::Compara(void) :
m_fich1(0),
m_fich2(0)
{
	m_validaciones.push_back(&Compara::Validacion1);
	m_validaciones.push_back(&Compara::Validacion2);
	m_validaciones.push_back(&Compara::Validacion3);
	m_validaciones.push_back(&Compara::Validacion4);
}

Compara::~Compara(void)
{
}


void Compara::Run(const std::string& path_result)
{
	cmp(m_fich1, m_fich2, path_result);	
}


bool Compara::Validacion1(Fichero_t *fich1, Fichero_t *fich2, Results_t& results)
{
	Results_t_t res;
	if (fich1->SizeLines() != fich2->SizeLines())
	{
		res.detalles = "El numero de lineas es difiere de fichero a fichero, se compara solo el n� de lineas del mas peque�o.";
		res.estatus = std::make_pair(false, WARNING);
		results.push_back(res);		
		return false;
	}
	return true;
}

bool Compara::Validacion2(Fichero_t *fich1, Fichero_t *fich2, Results_t& results)
{
	Results_t_t res;
	if (fich1->SizeBytes() != fich2->SizeBytes())
	{
		res.detalles = "El numero de bytes difiere de un fichero al otro.";
		res.estatus = std::make_pair(false, WARNING);
		results.push_back(res);
		return false;
	}
	return true;
}


bool Compara::Validacion3(Fichero_t *fich1, Fichero_t *fich2, Results_t& results)
{
	Results_t_t res;
	Campos_t campos_fich1 = fich1->GetCampos();
	Campos_t campos_fich2 = fich2->GetCampos();

	for(size_t i = 0; i < campos_fich1.size(); i++)
	{
		if (campos_fich1[i].first.compare(campos_fich2[i].first) != 0)
		{
			std::stringstream ss;
			ss << "Los nombre de campo diferen en el catalogo : " << campos_fich1[i].first << " <> " << campos_fich2[i].first;
			res.detalles = ss.str();
			res.estatus = std::make_pair(false, ERROR);
			results.push_back(res);
			return false;
		}
		if (campos_fich1[i].second != campos_fich2[i].second)
		{
			std::stringstream ss;
			ss << "La longitud de campos diferen en la definicion de su fichero de estructura: " << campos_fich1[i].first;
			ss << "[" << campos_fich1[i].second << "]" << " <> " ;
			ss << campos_fich2[i].first << "[" << campos_fich1[i].second << "]";
			res.detalles = ss.str();
			res.estatus = std::make_pair(false, ERROR);
			results.push_back(res);
			return false;
		}
	}

	return true;
}

bool Compara::Validacion4(Fichero_t *fich1, Fichero_t *fich2, Results_t& results)
{
	Results_t_t res;
	Campos_t campos_fich1 = fich1->GetCampos();
	Campos_t campos_fich2 = fich2->GetCampos();

	if(campos_fich1.size() != campos_fich2.size())
	{
		res.detalles = "El catalogo de campos es deferente de un fichero al otro.";
		res.estatus = std::make_pair(false, ERROR);
		results.push_back(res);
		return false;
	}

	size_t cols = fich1->numColFromDatos();
	if(campos_fich1.size() != cols)
	{
		res.detalles = "El numero de columnas del fichero 1 es diferente al de su fichero de estructura";
		res.estatus = std::make_pair(false, ERROR);
		results.push_back(res);
		return false;
	}	

	if(campos_fich2.size() != cols)
	{
		res.detalles = "El numero de columnas del fichero 2 es diferente al de su fichero de estructura";
		res.estatus = std::make_pair(false, ERROR);
		results.push_back(res);
		return false;
	}	

	return true;
}

bool Compara::Validaciones(Fichero_t *fich1, Fichero_t *fich2, Results_t& results)
{
	for(size_t i = 0; i < m_validaciones.size(); i++)
		(this->*m_validaciones[i])(fich1, fich2, results);

	for(Results_t::iterator it = results.begin(); it != results.end(); it++)
	{ 
		if (it->estatus.first == false && it->estatus.second == ERROR) 
		{
			std::cout << "ERROR : " << it->detalles << std::endl;
			return false; 
		}
		std::cout << "WARNING : " << it->detalles << std::endl;
	}

	return true;
}


const char* Compara::srchLine(const char* init, const char* end, const char* address)
{
	const char* middle = getMiddle(init, end);
	if (middle == NULL)
	{
		
	}
	else if (address > middle)
	{
		srchLine(middle, end, address);
	}
	else if (address > middle)
	{
		srchLine(init, middle, address);
	}

	return NULL;
}

bool Compara::nuevo_cmp(Fichero_t* , Fichero_t* , const std::string& )
{
	/*
	const char* cinit = fich1->GetPtrLine(0);
	const char* cend = fich1->GetPtrLine(fich2->SizeLines());	
	const char* cmmidle = cinit + ((cend - cinit)/2);
	*/
	return true;
}

inline const char* Compara::getMiddle(const char* init, const char* end)
{	
	return ((end - init) == 0)? NULL : init + ((end - init)/2);
}

bool Compara::cmp(Fichero_t* fich1, Fichero_t* fich2, const std::string& path_result)
{
	Results_t results;
	if (Validaciones(fich1, fich2, results) == false) return false;

	Results_t_t res;
	const Fichero_t* ptr = 0;
	ResultsStatus_t resStat;	

	ptr = (fich1->SizeLines() > fich2->SizeLines()) ? fich1 : fich2;	
	Campos_t campos = fich1->GetCampos();
	size_t x = 0, TotalLineas = ptr->SizeLines(), i = 0;
	Campos_t::iterator it;
	std::string val1, val2;
	std::stringstream ss;
	try
	{
		std::ofstream os(path_result.c_str());
		if (os.is_open())
		{
			os << "FICHEROS COMPARADOS :" << std::endl << fich1->GetPathDatos() << std::endl << fich2->GetPathDatos() << std::endl << std::endl;
			os << ss.str();
			ss << "DETALLES" << '\t';
			ss << "N� DE LINEA AFECTADA" << '\t';
			ss << "CAMPO AFECTADO" << '\t';
			ss << "VALOR FICHERO 1" << '\t';
			ss << "VALORE FICHERO 2" << std::endl;
			os << ss.str();
			for(i = 0; i <= TotalLineas; i++)
			{
				const char* const ptrLine1 = fich1->GetPtrLine(i);
				const char* const ptrLine2 = fich2->GetPtrLine(i);
				while ( *(ptrLine1+x) != '\n') 
				{ 
					if (*(ptrLine1+x) != *(ptrLine2+x))
					{

						for(it = campos.begin(); it != campos.end(); it++)
						{						
							val1 = fich1->getValor(i, it->first);
							val2 = fich2->getValor(i, it->first);
							if (val1.compare(val2))
							{
								ss.str(" ");
								ss << "DIFIEREN" << '\t' << i+1 << '\t';
								ss << it->first << '\t'  << fich1->getValor(i, it->first) << '\t';
								ss << fich2->getValor(i, it->first) << std::endl;
								os << ss.str();
							}
						}
						break;
					}
					x++; 
				}
				if (i % 1000 == 0)
					std::printf("Numero de registros tratados %d de %d.\n", i, TotalLineas);
				x = 0;
			}
			os.close();
		}
	}
	catch (const std::exception &e)
	{
		std::printf("Se ha producido un error. Descripcion Error : %s\n\n", e.what());
		std::printf("TRACE INI =================================================================\n");
		std::printf("LINEA : %d\nDESP. CARACTER : %d\n", i, x);	
		std::printf("CAMPO: %s\nVALOR1: %s\nVALOR2: %s\n", 
			it->first.c_str(), val1.c_str(), val2.c_str());
		std::printf("TRACE FIN =================================================================\n");
		std::exit(-1);
	}
	std::printf("Numero de registros tratados %d de %d.\n", TotalLineas, TotalLineas);
	return true;
}
