#ifndef COMPARA_H_
#define COMPARA_H_
/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *--------------------------------------------------------------------------------------------------------------*/
#include <fstream>
#include <vector>
#include <sstream>
#include <utility>
#include "Fichero_t.h"

/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *--------------------------------------------------------------------------------------------------------------*/
#define OBSOLETO
#define TEST

class Compara;
/*--------------------------------------------------------------------------------------------------------------
 * TYPES
 *--------------------------------------------------------------------------------------------------------------*/
	typedef std::pair<bool, char>									ResultsStatus_t;
	typedef struct													Results_t_t
	{
		ResultsStatus_t			estatus;
		std::string				detalles;
		int						num_linea;
		std::string				campo;
		std::string				valor_fich1;
		std::string				valor_fich2;
	} Results_t_t;
	typedef	std::vector<Results_t_t>								Results_t;	
	typedef bool (Compara::*ptrM_t_t) ( Fichero_t*, Fichero_t*, Results_t& );
	typedef std::vector<ptrM_t_t>										ptrM_t;
	

class Compara
{
/*--------------------------------------------------------------------------------------------------------------
 * PRIAVTE
 *--------------------------------------------------------------------------------------------------------------*/
private:
	Fichero_t*				m_fich1;		
	Fichero_t*				m_fich2;		
	enum					{ OK, WARNING, ERROR, DIFF};
	ptrM_t					m_validaciones;



							Compara(Compara&)						{}
	Compara					operator=(Compara&)						{}
	
	bool					nuevo_cmp								(Fichero_t* fich1, Fichero_t* fich2, const std::string& path_result);
	bool					cmp										(Fichero_t* fich1, Fichero_t* fich2, const std::string& path_result);
	bool					Validaciones							(Fichero_t* fich1, Fichero_t* fich2, Results_t& results);
	bool					Validacion1								(Fichero_t* fich1, Fichero_t* fich2, Results_t& results);
	bool					Validacion2								(Fichero_t* fich1, Fichero_t* fich2, Results_t& results);
	bool					Validacion3								(Fichero_t* fich1, Fichero_t* fich2, Results_t& results);
	bool					Validacion4								(Fichero_t* fich1, Fichero_t* fich2, Results_t& results);
	const char*				getMiddle								(const char* init, const char* end);
	const char*				srchLine								(const char* init, const char* end, const char* address);

/*--------------------------------------------------------------------------------------------------------------
 * PUBLIC
 *--------------------------------------------------------------------------------------------------------------*/
public:
							Compara(void);
							~Compara(void);
	void					Add										(Fichero_t* fich1, Fichero_t* fich2) { m_fich1 = fich1; m_fich2 = fich2; }		
	void					Run										(const std::string& path_result);
};

#endif //COMPARA_H_
