#include "Fichero_t.h"

Fichero_t::Fichero_t(Fichero_t& ) :
	m_ptr(0),
	m_size(0),
	m_path_struc(0),
	m_path_datos(0),
	m_sep('\0'),
	m_sep_linea_unix('\0'),
	m_tipo('\0')

{
}


Fichero_t::Fichero_t(const Path_t& path_struc, const Path_t& path_datos, const char tipo, const char sep) :
	m_ptr(0),
	m_size(0),
	m_path_struc(path_struc),
	m_path_datos(path_datos),
	m_sep(sep),
	m_sep_linea_unix('\n'),
	m_tipo(tipo)

{
	if (cargaEstructura(path_struc, m_campos) == 0)
	{
		if (cargaDatos(path_datos) != 0)
			limpiar_ptr();		
	} 
	else
		m_campos.clear();
}

const std::string Fichero_t::getValor(const int linea, const std::string& campo)
{
	return (m_tipo == 'S') ? getValorConSep(linea, utls::Trim(campo)) : getValorSinSep(linea, utls::Trim(campo));
}

bool Fichero_t::GetLinea(const int num_linea, std::string& linea)
{
	if(num_linea >= static_cast<int>(m_ptrs.size())) return false;
	char* ptr = m_ptrs.at(num_linea);
	linea.append(ptr, LineLen(ptr)-1);		

	return true;
}

inline int Fichero_t::LineLen(const char *ptr) const
{
	size_t len = 0;
	while(*(ptr+len++) != m_sep_linea_unix) {}
	return len;
}

const std::string Fichero_t::ViewLine(int num_linea)
{
	std::stringstream ss;	
	for(Campos_t::iterator it = m_campos.begin(); it != m_campos.end(); it++)
		ss << (*it).first << " : " << getValor(num_linea, (*it).first) << std::endl;
	return ss.str();
}

inline const std::string Fichero_t::getValorConSep(const int linea, const std::string& campo)
{
	assert(ExistCampo(campo) == true);
	assert(linea < (int)m_ptrs.size());	
	size_t pos = 0;

	std::string ret;
	//assert(GetLinea(linea, ret) == true);
	GetLinea(linea, ret);
	std::vector<std::string> campos;
	
	while((pos = ret.find_first_of(m_sep)) != std::string::npos)
	{
		campos.push_back(ret.substr(0, pos));
		ret = ret.substr(pos+1, ret.size());
	}
	campos.push_back(utls::Trim(ret));
	return campos.at(sizeCampo(campo).second);
}

inline const std::string Fichero_t::getValorSinSep(const int linea, const std::string& campo)
{	
	assert(linea < static_cast<int>(m_ptrs.size()));
	std::string ret;
	GetLinea(linea, ret);
	if (ret.size() > 0)
		ret = ret.substr(getDesplazamiento(campo), sizeCampo(campo).first);

	return utls::Trim(ret);
}

inline bool Fichero_t::ExistCampo(const std::string &nombre_campo)
{
	for(Campos_t::iterator it = m_campos.begin(); it != m_campos.end(); it++)
	{	if ((*it).first.compare(nombre_campo) == 0) return true; }

	return false;
}

int Fichero_t::getDesplazamiento(const std::string& campo)
{
	int desp = 0;
	for(Campos_t::iterator it = m_campos.begin(); it != m_campos.end() && ((*it).first.compare(campo)) != 0; it++)
		desp += (*it).second;	

	return desp;
}

inline size_t Fichero_t::numCampos() const
{
	return m_campos.size();
}

size_t Fichero_t::numColFromDatos()
{
	size_t cols = 0, x = 0;
	while ( *(m_ptr+x) != '\n')
	{
		if (*(m_ptr+x) == '\t')
			cols++;
		x++;
	}

	return cols+1;
}


inline Pos_t Fichero_t::sizeCampo(const std::string& nombre_campo)
{	
	size_t i = 0;
	for(Campos_t::iterator it = m_campos.begin(); it != m_campos.end(); it++, i++)
	{
		if ((*it).first.compare(nombre_campo) == 0)
			return std::make_pair((*it).second, i);
	}

	return  std::make_pair(0, 0);
}

Fichero_t::~Fichero_t(void)
{
	limpiar_ptr();
}

int Fichero_t::cargaDatos(const Path_t& path_datos)
{	
	std::ifstream inFile(path_datos);
	if (inFile.fail())
		return -1;

	inFile.seekg( 0, std::ios::end );
	long fileSize = inFile.tellg();
	inFile.seekg( 0, std::ios::beg );

	m_ptr = new char[fileSize+1];
	memset(m_ptr, 0, fileSize+1);
	inFile.read( m_ptr, fileSize );
	inFile.close();
	m_size = fileSize;

	// Se reajusta el tama�o
	size_t i = 0;
	char* ptr = m_ptr;
	m_ptrs.push_back(ptr);
	while(i++ < m_size) 
	{ 
		if (*(ptr+i) == m_sep_linea_unix)
			m_ptrs.push_back(ptr+i+1);
	}
	size_t size_ocup = i - 1;
	while(*(ptr+(size_ocup)) == '\0') {size_ocup--;}
	if (*(ptr+size_ocup) == m_sep_linea_unix)
		m_ptrs.erase(m_ptrs.begin()+(m_ptrs.size()-1));
	
	return 0;
}

int Fichero_t::cargaEstructura(const Path_t& path_struc, Campos_t& campos)
{
	std::ifstream inFile(path_struc, std::ios::in);
	if (inFile.fail())
		return 1;

	while(inFile.good())
	{
		std::string line;
		std::getline(inFile, line);
		std::string first = line.substr(0, line.find("\t"));
		if (first.empty()) continue;
		std::stringstream ss(line.substr( line.find("\t"), line.size() ) );
		int second = 0;
		ss >> second;
		campos.push_back(std::make_pair(utls::Trim(first), second));		
	}	
	inFile.close();
	if (campos.size() == 0) return 2;

	return 0;
}

void Fichero_t::limpiar_ptr()
{
	if (m_ptr != 0)
	{
		delete[] m_ptr;
		m_ptr = 0;
	}
}
