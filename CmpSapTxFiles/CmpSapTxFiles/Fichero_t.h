#ifndef FICHERO_T_H_
#define FICHERO_T_H_

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *--------------------------------------------------------------------------------------------------------------*/
#include <cassert>
#include <cstring>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>

#include "utils.hpp"

/*--------------------------------------------------------------------------------------------------------------
 * TYPES
 *--------------------------------------------------------------------------------------------------------------*/
typedef	char*											Ptr_t;
typedef std::vector<char*>								Ptrs_t;
typedef std::vector< std::pair< std::string, int > >	Campos_t;
typedef	char*											Path_t;
typedef std::pair<int, size_t>							Pos_t;

class Fichero_t
{

/*--------------------------------------------------------------------------------------------------------------
 * PRIAVTE
 *--------------------------------------------------------------------------------------------------------------*/
private:
	Ptr_t				m_ptr;
	size_t				m_size;
	Campos_t			m_campos;
	const Path_t		m_path_struc;
	const Path_t		m_path_datos;
	const char			m_sep;	
	const char			m_sep_linea_unix;
	Ptrs_t				m_ptrs;
	const char			m_tipo;
	
	int					cargaEstructura			(const Path_t& path_struc, Campos_t& campos);
	int					cargaDatos				(const Path_t& path_datos);
	void				limpiar_ptr				();
	const std::string	getValorConSep			(const int linea, const std::string& campo);
	const std::string	getValorSinSep			(const int linea, const std::string& campo);
	int					LineLen					(const char* ptr) const;

						Fichero_t				(Fichero_t& );
	Fichero_t			operator=				(Fichero_t& ) {}

/*--------------------------------------------------------------------------------------------------------------
 * PUBLIC
 *--------------------------------------------------------------------------------------------------------------*/
public:

						Fichero_t				(const Path_t& path_struc, const Path_t& path_datos, const char tipo, const char sep = ' ');
						~Fichero_t				(void);

	size_t				numCampos				() const;
	size_t				numColFromDatos			();
	size_t				SizeBytes				() const { return m_size; }
	size_t				SizeLines				() const { return m_ptrs.size()-1; }
	Pos_t				sizeCampo				(const std::string& nombre_campo);
	bool				ExistCampo				(const std::string& nombre_campo);
	const Campos_t		GetCampos				() const { return m_campos; }
	const std::string	getValor				(const int linea, const std::string& campo);
	bool				GetLinea     			(const int num_linea, std::string& linea);
	int					getDesplazamiento		(const std::string& campo);
	const std::string	ViewLine				(int num_linea);
	const char* const   GetPtrLine				(size_t i) { return m_ptrs.at(i); }
	const char*			GetPathDatos			() { return m_path_datos; }
	const char*			GetPathStruct			() { return m_path_struc; }

};

#endif // FICHERO_T_H_
