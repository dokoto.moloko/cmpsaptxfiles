#include "main.hpp"

/*--------------------------------------------------------------------------------------------------------------
 * Main
 *--------------------------------------------------------------------------------------------------------------*/
int main(const int argc, const char* argv[])
{		
	#ifdef _MSC_VER
		std::locale::global(std::locale("spanish")); 
		std::wcout.imbue(std::locale("spanish"));	
	#endif
	#ifndef _MSC_VER
		std::locale::global(std::locale("spanish")); 
		std::cout.imbue(std::locale("es_ES.UTF-8"));	
	#endif	
	Main main;
	if (main.CheckArgs(argc, argv) == false) return false;

	return main.Run();	
}






/*--------------------------------------------------------------------------------------------------------------
 * Main::Main
 *--------------------------------------------------------------------------------------------------------------*/

Main::Main(void) :
	m_fich1(0),
	m_fich2(0),
	m_obg(6)
{	
	
}

Main::~Main(void)
{
	if (m_fich1 != 0)
	{
		delete m_fich1;
		m_fich1 = 0;
	}
	if (m_fich2 != 0)
	{
		delete m_fich2;
		m_fich2 = 0;
	}
}

bool Main::Run(void)
{	
	CargaFicheros(m_fich1, m_fich2);
	CompararFicheros(m_fich1, m_fich2, m_args["-fich_result"]);
	int stop = 0;
	std::cout << std::endl << "Pulse cualquier tecla para finalizar...";
	std::cin.get();
	return true;
}

bool Main::CargaFicheros(Fichero_t*& fich1, Fichero_t*& fich2)
{
	fich1 = new Fichero_t(const_cast<Path_t>(m_args["-fich1_struct"].c_str()), const_cast<Path_t>(m_args["-fich1_datos"].c_str()), *m_args["-tipo"].c_str(), *m_args["-sep"].c_str());
	fich2 = new Fichero_t(const_cast<Path_t>(m_args["-fich2_struct"].c_str()), const_cast<Path_t>(m_args["-fich2_datos"].c_str()), *m_args["-tipo"].c_str(), *m_args["-sep"].c_str());
	
	return true;
}

bool Main::CompararFicheros(Fichero_t* fich1, Fichero_t* fich2, const std::string& path_result)
{
	m_comp.Add(fich1, fich2);	
	m_comp.Run(path_result);

	return true;
}


bool Main::CheckArgs(const int argn, const char* argv[])
{
	int oblg = 0;
	if (CargaArgs(argn, argv) == false) return false;
        
    for ( Args_t::iterator it = m_args.begin(); it != m_args.end(); it++)
    {
        if (((*it).first).compare("-fich1_datos") == 0)
		{
			if (Validacion1((*it).second) == false) return false;
			if (Validacion4((*it).second) == false) return false;
			oblg++;         
		}
        else if (((*it).first).compare("-fich1_struct") == 0)        
		{
			if (Validacion1((*it).second) == false) return false;
			if (Validacion4((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-fich2_datos") == 0)
		{
			if (Validacion1((*it).second) == false) return false;
			if (Validacion4((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-fich2_datos") == 0)
		{
			if (Validacion1((*it).second) == false) return false;
			if (Validacion4((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-fich2_struct") == 0)
		{
			if (Validacion1((*it).second) == false) return false;
			if (Validacion4((*it).second) == false) return false;
            oblg++;
		}		
		else if (((*it).first).compare("-fich_result") == 0)
		{
			if (Validacion4((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-tipo") == 0)
		{
			if (Validacion2((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-sep") == 0)
		{
			if (Validacion3((*it).second) == false) return false;
            continue;
		}
        else if (((*it).first).compare("-help") == 0)
		{
			std::cerr <<  m_use << std::endl;
            return false;
		}
		else if (((*it).first).compare("prog") == 0)
			continue;
        else
		{

            std::cerr <<  "Parametros no reconocido. Use --help." << std::endl;
            return false;
		}
    }	

	if (oblg != m_obg)
	{
		std::cerr <<  "Faltan parametros obligatorios. Use --help." << std::endl;
        return false;
	}

	return true;
}

bool Main::Validacion4(const std::string& valor)
{
    std::string like_unix = valor.substr(0, 1);
    std::string like_win = valor.substr(1, 2);
    if ( ( like_unix.compare("/") != 0 ) && ( like_win.compare(":\\") != 0 ) )
	{
		std::stringstream ss;
		ss << "No se reconoce el tipo de la ruta al fichero UNIX o WIN : " << valor << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}

	return true;
}

bool Main::Validacion1(const std::string& valor)
{
	std::ifstream inFile(valor.c_str());
	if (inFile.fail())
	{
		std::stringstream ss;
		ss << "El fichero no parece existir en : " << valor << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}

	inFile.close();

	return true;
}

bool Main::Validacion2(const std::string& valor)
{
	if ( ( valor.compare("F") != 0 ) && ( valor.compare("S") != 0 ) )
	{
		std::stringstream ss;
		ss << "Tipo no reconocido : " << valor << " , Valores permitidos: 'F' 'S'." << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}

	return true;
}

bool Main::Validacion3(std::string& valor)
{
	if (valor.empty())
	{
		std::stringstream ss;
		ss << "No se ha indicado el caracter separador." << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}

	return true;
}

bool Main::CargaArgs(const int argn, const char *argv[])
{
	std::string prog_name(argv[0]);
	std::string like_unix = prog_name.substr(0, 1);
    std::string like_win  = prog_name.substr(1, 2);
	
    if ( like_unix.compare("/") == 0 )
		prog_name.substr(prog_name.find_last_of("/")+1, prog_name.size());
	else if ( like_win.compare(":\\") == 0 ) 
		prog_name = prog_name.substr(prog_name.find_last_of("\\")+1, prog_name.size());
	

    size_t pos = 0;
    while ((pos = m_use.find("%s")) != std::string::npos)
		m_use.replace(pos, 2, prog_name);

    if (argn == 1)
    {
		std::cerr <<  m_use << std::endl;
		return false;
    }
    
    //for (size_t i = 0; i < static_cast<size_t>(argn-1);)
	for (size_t i = 0; i < SC(size_t, argn-1);)
    {
        if (i == 0)
        {
            m_args["prog"] = std::string(argv[i]);
            i++;
        } else
        {
			m_args[std::string(argv[i])] = std::string(argv[i+1]);
            i += 2;
        }
    } 

	if ((m_args.find("-sep")) == m_args.end())
		m_args["-sep"] = "\t";

	return true;
}


