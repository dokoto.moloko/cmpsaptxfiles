#ifndef MAIN_HPP_
#define MAIN_HPP_

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *--------------------------------------------------------------------------------------------------------------*/
#include <cassert>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Fichero_t.h"
#include "Compara.h"

/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *--------------------------------------------------------------------------------------------------------------*/
#define SC(Tipo, Valor) static_cast<Tipo>(Valor)
/*--------------------------------------------------------------------------------------------------------------
 * TYPES
 *--------------------------------------------------------------------------------------------------------------*/
typedef std::map<std::string, std::string>					Args_t;

class Main
{
/*--------------------------------------------------------------------------------------------------------------
 * PRIVATE
 *--------------------------------------------------------------------------------------------------------------*/
private:
	Fichero_t*					m_fich1;
	Fichero_t*                  m_fich2;
	Results_t					m_results;
	Compara						m_comp;
	Args_t						m_args;
	static std::string			m_use;
	const int					m_obg;

	bool						CargaFicheros				(Fichero_t*& fich1, Fichero_t*& fich2);	
	bool						CompararFicheros			(Fichero_t* fich1, Fichero_t* fich2, const std::string& path_result);	
	bool						CargaArgs					(const int argn, const char* argv[]);
	bool						Validacion1					(const std::string& valor);
	bool						Validacion2					(const std::string& valor);
	bool						Validacion3					(std::string& valor);
	bool						Validacion4					(const std::string& valor);


/*--------------------------------------------------------------------------------------------------------------
 * PUBLIC
 *--------------------------------------------------------------------------------------------------------------*/
public:
									Main						(void);
									~Main						(void);
	bool							CheckArgs					(const int argn, const char* argv[]);
	bool							Run							(void);

};

std::string Main::m_use = "Programa para comparar ficheros de texto generados por SAP.\n\
El programa necesita de dos fichero por fichero a comparar, uno con los datos a comparar y otro\n\
con la estructura o definicion del catalogo de campos del fichero a comparar. Los catalogos de\n\
ambos ficheros deben iguales y se componen de una columna con el nombre del campo y otra columna\n\
con el tama�o en digitos del campo separadas ambas columnas por tabuladores. En cuanto al fichero\n\
de datos, dos formatos son adminitidos un fichero de texto separado por un charater especificado,\n\
preferiblemente tabulador o un fichero de texto de longitudes de campo fijas donde el valore de un\n\
campo enpieza donde acaba su predecesor y donde los espacion en blanco aparecen.\n\n\
*****  EJEMPLOS DE FICHEROS *****\n\
Fichero de estructura:\n\
CAMPO01 <TABULADOR> 10\n\
CAMPO02 <TABULADOR> 4\n\
CAMPO03 <TABULADOR> 1\n\
CAMPO04 <TABULADOR> 15\n\n\
Fichero de Datos con separador:\n\
VALOR1 <TABULADOR> VALOR2 <TABULADOR> VALOR3\n\
VALOR1 <TABULADOR> VALOR2 <TABULADOR> VALOR3\n\
VALOR1 <TABULADOR> VALOR2 <TABULADOR> VALOR3\n\n\
Fichero de Datos con longitudes Fijas(los guines sumulan espacios):\n\
CAMPO01: VALOR1\n\
CAMPO02: VAL2\n\
CAMPO03: V\n\
CAMPO04: VALOR4\n\n\
VALOR1----VAL2VVALOR4---------\n\
VALOR1----VAL2VVALOR4---------\n\
VALOR1----VAL2VVALOR4---------\n\
VALOR1----VAL2VVALOR4---------\n\n\n\
*****  ARGUMENTOS *****\n\
Argumentos OBLIGATORIOS\n\
-fich1_datos  : Ruta entre comillas dobles al primer fichero con los datos a comparar.\n\
-fich1_struct : Ruta entre comillas dobles al primer fichero con los datos de estructura.\n\
-fich2_datos  : Ruta entre comillas dobles al segundo fichero con los datos a comparar.\n\
-fich2_struct : Ruta entre comillas dobles al segundo fichero con los datos de estructura.\n\n\
-fich_result  : Ruta entra comillas dobles la fichero de texto sep. por tabulaciones con los resultados.\n\
-tipo		  : 'F' = Indica que se usa estructura fija.  'S' = Indica que se usa separador\n\
Argumentos Opcionales\n\
-sep		  : Indicar el tipo de caracter separador: vacio = Tabulador(es el valor por defecto). ATENCION !!!\n\
	            si se usa como caracter separador un valor letra el comportamiento no esta predecido.\n\
-help  : Muestra esta ayuda\n\
Sin Parametros : Muestra esta ayuda\n\n\n\
*****  EJEMPLOS DE USO *****\n\
%s -fich1_datos \"D:\\TMP\\FICHERO\\fichero1_datos.txt\" -fich1_struct \"D:\\TMP\\FICHERO\\fichero1_struct.txt\"\n\
   -fich2_datos \"D:\\TMP\\FICHERO\\fichero2_datos.txt\" -fich2_struct \"D:\\TMP\\FICHERO\\fichero2_struct.txt\"\n\
   -fich_result \"D:\\TMP\\FICHERO\\fichero_resultados.xls\" -tipo=S -sep\n\n\
%s -fich1_datos \"D:\\TMP\\FICHERO\\fichero1_datos.txt\" -fich1_struct \"D:\\TMP\\FICHERO\\fichero1_struct.txt\"\n\
   -fich2_datos \"D:\\TMP\\FICHERO\\fichero2_datos.txt\" -fich2_struct \"D:\\TMP\\FICHERO\\fichero2_struct.txt\"\n\
   -fich_result \"D:\\TMP\\FICHERO\\fichero_resultados.xls\" -tipo S -sep |\n\n\
%s -fich1_datos \"D:\\TMP\\FICHERO\\fichero1_datos.txt\" -fich1_struct \"D:\\TMP\\FICHERO\\fichero1_struct.txt\"\n\
   -fich2_datos \"D:\\TMP\\FICHERO\\fichero2_datos.txt\" -fich2_struct \"D:\\TMP\\FICHERO\\fichero2_struct.txt\"\n\
   -fich_result \"D:\\TMP\\FICHERO\\fichero_resultados.xls\" -tipo F -sep\n\n\
";

#endif // 
