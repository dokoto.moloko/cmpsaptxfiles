#include "utils.hpp"

namespace utls
{
	std::string Trim(const std::string& str)
	{
		if (str.empty()) return std::string("");
		size_t pos_ini = 0, pos_fin = 0;
		std::string str_new;
		pos_ini = str.find_first_not_of(" ");
		str_new = str.substr(pos_ini, str.size() - pos_ini);
		pos_fin = str_new.find_last_not_of(" ");		
		return str_new.erase(pos_fin+1, str_new.size());
	}
}