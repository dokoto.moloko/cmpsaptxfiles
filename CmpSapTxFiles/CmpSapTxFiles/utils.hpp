#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>

namespace utls
{
	std::string Trim(const std::string& str);
}

#endif // UTILS_HPP

